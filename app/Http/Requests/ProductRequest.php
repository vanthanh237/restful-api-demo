<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6|max:150',
            'price' => 'required|max:7',
            'description' => 'required|min:6|max:255'
        ];

    }

    public function messages()
    {
        return [
            'required' => ':attribute not be empty!',
            'max' => ':attribute exceed the number of characters!',
            'min' => ':attribute the number of characters is too short'
        ];
    }

    protected function failedValidation(Validator $validator) 
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(
            [
                'status_code' => 422,
                'error' => $errors,
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
