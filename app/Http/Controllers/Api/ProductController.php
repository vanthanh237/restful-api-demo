<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->all();
        $products_resource = ProductResource::collection($products);
        return $this->sendResponse($products_resource, 'Show a list of successful products!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $create_product = $this->product->create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description
        ]);
        $product_resource = new ProductResource($create_product);
        return $this->sendResponse($product_resource, "Create a successful new product!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->findOrFail($id);
        $product_resource = new ProductResource($product);
        return $this->sendResponse($product_resource, 'Successful product display!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product = $this->product->findOrFail($id);
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description
        ]);
        $product_resource = new ProductResource($product);
        return $this->sendResponse($product_resource, 'Product update successful!');
        // $product = Product::findOrFail($id);
        // $product->update($request->all());
        // $product_resource = new ProductResource($product);
        // return $this->sendResponse($product_resource, 'Product update successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $product_resource = new ProductResource($product);
        $product->delete();
        return $this->sendResponse($product_resource, 'Delete product successfully!');
    }
}
